<%-- 
    Document   : muestraEmpleado
    Created on : Dec 20, 2016, 12:06:55 PM
    Author     : benjsant
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="empleado" scope="request" class="packageData.Empleado" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="./css/estilos.css">
        <title>JavaBean</title>
    </head>
    <body>
        <h1>DATOS DEL EMPLEADO</h1>
        <table>
            <tr><td>DNI:</td><td><jsp:getProperty name="empleado" property="dni" /></td></tr>
            <tr><td>Nombre:</td><td><jsp:getProperty name="empleado" property="nombre" /></td></tr>
            <tr><td>Apellidos:</td><td><jsp:getProperty name="empleado" property="apellidos" /></td></tr>
            <tr><td>Edad:</td><td><jsp:getProperty name="empleado" property="edad" /></td></tr>
        </table>
        <br><br><br>
        <a href="index.html">INICIO</a>
    </body>
</html>
