<%-- 
    Document   : form_delete
    Created on : Dec 21, 2016, 9:24:42 AM
    Author     : benjsant
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="./css/estilos.css">
        <title>GESTIÓN DE EMPLEADOS</title>
    </head>
    <body>
        <h1>Gestión de Empleados</h1>
        <fieldset>
            <legend>CREAR EMPLEADO</legend>
            <form action="Insert" method="post">
                <table>
                    <tr>
                        <td>DNI: </td>
                        <td><input type="text" name="dni"></td>
                    </tr>
                    <tr>
                        <td>Nombre: </td>
                        <td><input type="text" name="nombre"></td>
                    </tr>
                    <tr>
                        <td>Apellidos: </td>
                        <td><input type="text" name="apellidos"></td>>
                    </tr>
                    <tr>
                        <td>Edad: </td><td><input type="text" name="edad"></td>
                    </tr>
                </table>
                <input type="submit" value="Crear"><br>
            </form><br><br>
            <a href="index.html">INICIO</a>
        </fieldset>
    </body>
</html>
