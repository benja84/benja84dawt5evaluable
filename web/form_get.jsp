<%-- 
    Document   : form_delete
    Created on : Dec 21, 2016, 9:24:42 AM
    Author     : benjsant
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="./css/estilos.css">
        <title>GESTIÓN DE EMPLEADOS</title>
    </head>
    <body>
        <h1>Gestión de Empleados</h1>
        <fieldset>
            <legend>BUSCAR EMPLEADO</legend>
            <form action="GetByDni" method="post"><br>
                DNI: <input type="text" name="dni"><br><br>
                <input type="submit" value="Buscar"><br>
            </form><br><br>
            <a href="index.html">INICIO</a>
        </fieldset>
    </body>
</html>
