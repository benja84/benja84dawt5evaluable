/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package packageData;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author benjsant
 */
public class AdminEmpleado {
    public void update(Connection con, Empleado empleado)throws Exception{
        PreparedStatement st = null;
        try {
            st = con.prepareStatement("update empleado set nombre=?, apellidos=?, edad=? where dni =?");
            st.setString(1,empleado.getNombre());
            st.setString(2,empleado.getApellidos());
            st.setInt(3, empleado.getEdad());
            st.setString(4,empleado.getDni());
            st.executeUpdate();
        }catch(SQLException e){
            throw new Exception("ERROR al actualizar el empleado: "+e.getMessage());
        }finally {
            if(st != null){
                st.close();
            }
        }
    }
    
    public void delete (Connection con, Empleado empleado) throws Exception{
        PreparedStatement st = null;
        try {
            st = con.prepareStatement("delete from empleado where dni =?");
            st.setString(1,empleado.getDni());
            st.executeUpdate();
        }catch(SQLException e){
            throw new Exception("ERROR al eliminar el empleado: "+e.getMessage());
        }finally {
            if(st != null){
                st.close();
            }
        }
    }
    
    public void insert(Connection con, Empleado empleado)throws Exception{
        PreparedStatement st = null;
        try {
            st = con.prepareStatement("insert into empleado (dni, nombre, apellidos, edad) values (?,?,?,?)");
            
            st.setString(1,empleado.getDni());
            st.setString(2,empleado.getNombre());
            st.setString(3,empleado.getApellidos());
            st.setInt(4, empleado.getEdad());
            
            st.executeUpdate();
        }catch(SQLException e){
            throw new Exception("ERROR al insertar el empleado: "+e.getMessage());
        }finally {
            if(st != null){
                st.close();
            }
        }
    }
    
    public Empleado GetByDni(Connection con, String dni) throws Exception{
        Empleado empleado = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            st = con.prepareStatement("select * from empleado where dni = " + dni);
            rs = st.executeQuery();
            while(rs.next()){
                empleado = new Empleado();
                empleado.setDni(rs.getString("dni"));
                empleado.setNombre(rs.getString("nombre"));
                empleado.setApellidos(rs.getString("apellidos"));
                empleado.setEdad(rs.getInt("edad"));
            }
            
        }catch(SQLException e){
            throw new Exception("ERROR al obtener el empleado: "+e.getMessage());
        } finally {
            if(rs != null) {
                rs.close();
            }
            if(st != null) {
                st.close();
            }
        }
        return empleado;
    } 
}
