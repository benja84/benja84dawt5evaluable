/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package packageData;

import java.sql.Connection;
import java.sql.SQLException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 *
 * @author benjsant
 */
public class ConnectionDB {
    public Connection OpenConnection() throws Exception {
        Connection con;
        try{
            Context ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/Empleado");
            con = ds.getConnection();
            return con;
        }catch(SQLException | NamingException e){
            throw new Exception ("Error al conectar con la Base de Datos: "+e.getMessage());
        }
    }
    
    public void CloseConnection(Connection con) throws Exception {
        try {
            if (con != null){
                con.close();
            }
        }catch(SQLException e){
            throw new Exception ("Error al cerrar la conexión con la Base de Datos: "+e.getMessage());
        }
    }
}
