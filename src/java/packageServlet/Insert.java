/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package packageServlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import packageData.AdminEmpleado;
import packageData.ConnectionDB;
import packageData.Empleado;

/**
 *
 * @author benjsant
 */
public class Insert extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            ConnectionDB conDB = new ConnectionDB();
            Connection con = conDB.OpenConnection();
            AdminEmpleado empleado2 = new AdminEmpleado();

            Empleado empleado = new Empleado();

            if (request.getParameter("dni") == "" || request.getParameter("nombre") == "" || 
                    request.getParameter("apellidos") == "" || request.getParameter("edad") == "") {
                out.print("ERROR, debe completar todos los campos");
                String link = "<br><br><a href=\"index.html\">INICIO</a>";
                out.print(link);
                
            } else {
                empleado.setDni(request.getParameter("dni"));
                empleado.setNombre(request.getParameter("nombre"));
                empleado.setApellidos(request.getParameter("apellidos"));
                empleado.setEdad(Integer.parseInt(request.getParameter("edad")));

                empleado2.insert(con, empleado);
                conDB.CloseConnection(con);

                RequestDispatcher rd = this.getServletContext().getRequestDispatcher("/form_insert.jsp");
                rd.forward(request, response);
            }

        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Insert.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Insert.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
